require 'test_helper'

class CompanyTest < ActiveSupport::TestCase

  test 'Show the relationship of Company to its users' do
    association = Company.reflect_on_association(:users)

    assert_not_nil association, 'Company should have association to users'
    assert_equal association.macro,
                :has_many,
                'A company should have many users'
  end

  test 'Show the relationship of Company to its lessons' do
    association = Company.reflect_on_association(:lessons)

    assert_not_nil association, 'Company should have association to lessons'
    assert_equal association.macro,
                 :has_many,
                 'A company should have many lessons'
  end

  test 'valid company can be saved' do
    name = 'Awesome Peoples 1'
    company = Company.new(name: name)

    assert company.save, company.errors.messages
  end

  test 'Company name must be less than or equal to 255 characters' do
    name = 'a' * 256
    company = Company.new(name: name)

    assert_not company.save
  end

  test 'Company name Must contain only letters or numbers' do
    name = '(1fUn^ky- T0{w}N~'
    company = Company.new(name: name)

    assert_not company.save
  end

  test 'Company name must be present' do
    nil_name = Company.new(name: nil)
    blank_name = Company.new(name: ' ')

    assert_not nil_name.save, 'nil name should not be allowed'
    assert_not blank_name.save, 'blank name should not be allowed'
  end

  test 'scope to order companies by name alphabetically' do
    names = %w(Zeltonite Wasabiscuits Artempative Crazitrons)
    names.each { |name| Company.create(name: name) }

    company_names = Company.alphabetically_by_name.map(&:name)
    assert_equal company_names, %w(Artempative Crazitrons Wasabiscuits Zeltonite)
  end

  test 'scope to get companies that are on customer plans' do
    Company.plans.keys.each do |plan|
      Company.create(name: plan, plan: plan)
    end
    customer_plans = Company.customer_plans

    assert_equal customer_plans, Company.where.not(plan: Company.plans["trialing"])
  end

  test 'scope to get companies that are currently active' do
    active_company = Company.create(name: 'active')
    deactivated_company = Company.create name: 'deactivated',
                                         deactivated_at: DateTime.now

    assert_equal Company.active.to_a, [active_company]
  end

  test 'scope to get companies that were created last month' do
    last_month = Date.today.last_month
    end_last_month = Company.create name: 'last month end',
                                    created_at: last_month.end_of_month
    start_last_month = Company.create name: 'last month start',
                                      created_at: last_month.beginning_of_month

    today = Company.create(name: 'freshco')

    assert_includes Company.created_last_month.to_a, start_last_month
    assert_includes Company.created_last_month.to_a, end_last_month
    refute_includes Company.created_last_month.to_a, today
  end
end
