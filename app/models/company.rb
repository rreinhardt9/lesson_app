class Company < ActiveRecord::Base
  ZERO_OR_MORE_NUMBERS_LETTERS_OR_SPACES = /\A[A-Za-z\d ]*\z/i

  has_many :users
  has_many :lessons
  validates :name,
            length: { maximum: 255 },
            format: {
              with: ZERO_OR_MORE_NUMBERS_LETTERS_OR_SPACES,
              message: 'must only contain letters, numbers, and spaces'
            },
            presence: true

  scope :alphabetically_by_name, -> { order(:name) }
  scope :customer_plans, -> { where.not(plan: 0) }
  scope :active, -> { where(deactivated_at: nil)  }
  scope :created_last_month, -> do
    where(created_at: Date.today.last_month.beginning_of_month..Date.today.beginning_of_month)
  end

  enum plan: {
    trialing: 0,
    legacy: 1,
    custom: 2,
    basic: 3,
    plus: 4,
    growth: 5,
    enterprise: 6
  }
end
