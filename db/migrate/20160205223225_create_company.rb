class CreateCompany < ActiveRecord::Migration
  def change
    enable_extension("citext")

    create_table :companies do |t|
      t.string :name
      t.citext :subdomain
      t.datetime :subscribed_at
      t.datetime :deactivated_at
      t.date :trial_expires_at
      t.integer :plan

      t.timestamps
    end
  end
end
